package com.company;

import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

public class Coffee extends Drinks {
    private String beansType;
    private String[] types = new String[]{"arabica", "robusta"};
    private String[] companyNames = new String[]{"Tchibo", "Nestle", "Jardin", "Jacobs"};
    private String[] productNames = new String[]{"roasted beans", "instant", "canned"};
    private String[] countries = new String[]{"Colombia", "Brazil", "Vietnam"};

    @Override
    public void create() {
        super.create();
        Random rand = new Random();
        int beansNumber, companyNumber, nameNumber, countryNumber;
        beansNumber = rand.nextInt(2);
        beansType = types[beansNumber];
        companyNumber = rand.nextInt(4);
        companyName = companyNames[companyNumber];
        nameNumber = rand.nextInt(3);
        productName = productNames[nameNumber];
        countryNumber = rand.nextInt(3);
        country = countries[countryNumber];
        price = rand.nextInt(400)+200;
    }

    @Override
    public void read() {
        System.out.print("Product ID: " + this.productID + "\n" + "Product name: " +
                this.productName + "\n" + "Price: " + this.price + "\n" + "Counter: " +
                counter + "\n" + "Company name: " + this.companyName + "\n" +
                "Country: " + this.country + "\n" + "Beans type: " + this.beansType + "\n");
    }

    @Override
    public void update() {
        System.out.println("Enter product name: ");
        Scanner in = new Scanner(System.in);
        this.productName = in.nextLine();
        System.out.println("Enter price: ");
        this.price = in.nextDouble();
        System.out.println("Enter company name: ");
        this.companyName = in.nextLine();
        System.out.println("Enter country: ");
        this.country = in.nextLine();
        System.out.println("Enter beans type: ");
        this.beansType = in.nextLine();
    }

    @Override
    public void delete(){
        super.delete();
        this.productID = null;
        this.productName = null;
        this.price = 0;
        this.companyName = null;
        this.country = null;
        this.beansType = null;
    }


    Coffee(){
        this.productID = UUID.randomUUID();
        this.productName = "name";
        this.price = 10;
        this.companyName = "company name";
        this.country = "country";
        this.beansType = "beans type";
    }

}
