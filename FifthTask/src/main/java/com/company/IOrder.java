package com.company;

import java.io.File;
import java.util.UUID;

public interface IOrder {
    void readById(File file, UUID id, Order order);
    void saveById(Orders orders, UUID id, File file);
    void readAll(File file, Orders orders);
    void saveAll(Orders orders, File file);
}
