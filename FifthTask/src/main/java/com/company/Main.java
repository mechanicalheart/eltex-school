package com.company;

import java.io.File;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        Orders orders = new Orders();


        AutomaticOrderGeneration generation = new AutomaticOrderGeneration(orders);
        Thread gen = new Thread(generation);
        gen.start();
        TimeUnit.SECONDS.sleep(4);
        generation.requestStop();

        orders.showAllOrders();
        ManagerOrderJSON m = new ManagerOrderJSON();
        m.saveAll(orders);
        TimeUnit.SECONDS.sleep(3);
        CheckWaitingOrders check = new CheckWaitingOrders(orders);
        DeleteProcessedOrders delete = new DeleteProcessedOrders(orders);
        ///Thread ch = new Thread(check);
        //ch.start();
        //Thread d = new Thread(delete);
        //d.start();
        
        orders.showAllOrders();
        //ScheduledExecutorService sh = Executors.newSingleThreadScheduledExecutor();
        //sh.scheduleWithFixedDelay(ch, 0, 5, TimeUnit.SECONDS);
        //ScheduledExecutorService sh2 = Executors.newSingleThreadScheduledExecutor();
        //sh2.scheduleWithFixedDelay(d, 0, 5, TimeUnit.SECONDS);
        File file = new File("orders.json");
        Orders orders2 = new Orders();
        m.readAll(file, orders2);
        orders2.showAllOrders();


    }
}
