package com.company;

import java.io.*;
import java.util.UUID;

public class ManagerOrderFile extends AManageOrder {
    @Override
    public void readById(File file, UUID id, Order order) {

        Orders orders = new Orders();

        try(FileInputStream fis = new FileInputStream(file)) {
            ObjectInputStream ois = new ObjectInputStream(fis);
            orders = (Orders)ois.readObject();
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        for(int i = 0; i < orders.getSize(); i++){

            if(orders.getOrder(i).orderID == id){
                order = orders.getOrder(i);
            }
        }

    }

    @Override
    public void readAll(File file, Orders orders) {

        try(FileInputStream fis = new FileInputStream(file)) {
            ObjectInputStream ois = new ObjectInputStream(fis);
            orders = (Orders)ois.readObject();
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    @Override
    public void saveById(Orders orders, UUID id, File file) {

        for(int i = 0; i < orders.getSize(); i++) {

            if(orders.getOrder(i).orderID == id){

                try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
                    oos.writeObject(orders.getOrder(i));
                }
                catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }

        }

    }


    @Override
    public void saveAll(Orders orders, File file) {

        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(orders);
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }
}
