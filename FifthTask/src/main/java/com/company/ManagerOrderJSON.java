package com.company;

import com.google.gson.Gson;

import java.io.*;
import java.util.UUID;

public class ManagerOrderJSON extends AManageOrder {

    @Override
    public void readById(File file, UUID id, Order order) {

        Orders orders = new Orders();

        try (FileReader reader = new FileReader(file)) {

            orders = new Gson().fromJson(reader, Orders.class);

        } catch (IOException e){
            e.printStackTrace();
        }

        for(int i = 0; i < orders.getSize(); i++){

            if(orders.getOrder(i).orderID == id){
                order = orders.getOrder(i);
            }
        }

    }

    @Override
    public void saveById(Orders orders, UUID id, File file) {

        Gson gson = new Gson();

        for(int i = 0; i < orders.getSize(); i++) {
            if(orders.getOrder(i).orderID == id) {

                try (FileWriter writer = new FileWriter(file)) {

                    gson.toJson(orders.getOrder(i), writer);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void readAll(File file, Orders orders) {

        try (FileReader reader = new FileReader(file)) {

            orders = new Gson().fromJson(reader, Orders.class);

        } catch (IOException e){
            e.printStackTrace();
        }

    }


    @Override
    public void saveAll(Orders orders, File file) {

        Gson gson = new Gson();

        try (FileWriter writer = new FileWriter(file)) {

            gson.toJson(orders, writer);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
