package com.company;

import java.util.Date;
import java.util.LinkedList;
import java.util.UUID;

public class Order {
    protected String[] orderStatuses = new String[]{"waiting", "processed"};
    protected String orderStatus;
    protected Long creationTime;
    protected Long waitingTime;
    protected Credentials credentials;
    protected ShoppingCart cart;
    protected UUID orderID;

    public Order(Credentials credentials, ShoppingCart cart) {
        this.credentials = credentials;
        this.cart = cart;
        this.orderID = UUID.randomUUID();
    }

    public Order(Credentials credentials, ShoppingCart cart, UUID id) {
        this.credentials = credentials;
        this.cart = cart;
        this.orderID = id;
    }
}
