package com.company;

import java.sql.Array;
import java.util.*;

public class ShoppingCart<T extends Drinks> {
    private List<T> cart;
    private Set<UUID> generatedIDs;

    public ShoppingCart() {
        this.cart = new ArrayList<T>();
        this.generatedIDs = new HashSet<UUID>();
    }

    public void add(T t){
        cart.add(t);
        generatedIDs.add(t.productID);
    };

    public void delete(T t){
        cart.remove(t);
    };

    public boolean searchID(UUID productID){
        if(generatedIDs.contains(productID))
            return true;
        else
            return false;
    }

    public void showAllObjects(){
        for(int i = 0; i < cart.size(); i++) {
            System.out.println("ID: " + cart.get(i).productID);
            System.out.println("Name: " + cart.get(i).productName);
            System.out.println("Price: " + cart.get(i).price);
            System.out.println("Company: " + cart.get(i).companyName);
            System.out.println("Country: " + cart.get(i).country);
            System.out.println("--------------------------");
        }
    }
}
