package com.company;

import java.util.Objects;
import java.util.UUID;

public abstract class Drinks implements ICrudAction {
    protected UUID productID;
    protected String productName;
    protected double price;
    static int counter;
    protected String companyName;
    protected String country;

    @Override
    public void create() {
        counter++;
    }

    @Override
    public void delete(){
        counter--;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Drinks)) return false;
        Drinks drinks = (Drinks) o;
        return Double.compare(drinks.price, price) == 0 &&
                Objects.equals(productID, drinks.productID) &&
                Objects.equals(productName, drinks.productName) &&
                Objects.equals(companyName, drinks.companyName) &&
                Objects.equals(country, drinks.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productID, productName, price, companyName, country);
    }
}
