package com.company;

import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

public class Tea extends Drinks {
    //private UUID productID;
    private String packageType;
    private String[] types = new String[]{"loose", "bags"};
    private String[] companyNames = new String[]{"Lipton", "Ahmad Tea", "Dilmah", "Greenfield"};
    private String[] productNames = new String[]{"Green", "Black", "Oolong", "Earl Grey", "Mint", "Fruit"};
    private String[] countries = new String[]{"Kenya", "India", "China"};

    @Override
    public void create() {
        super.create();
        Random rand = new Random();
        int typeNumber, companyNumber, nameNumber, countryNumber;
        typeNumber = rand.nextInt(2);
        packageType = types[typeNumber];
        companyNumber = rand.nextInt(4);
        companyName = companyNames[companyNumber];
        nameNumber = rand.nextInt(6);
        productName = productNames[nameNumber];
        countryNumber = rand.nextInt(3);
        country = countries[countryNumber];
        price = rand.nextInt(400)+200;
    }

    @Override
    public void read() {
        System.out.print("Product ID: " + this.productID + "\n" + "Product name: " +
                this.productName + "\n" + "Price: " + this.price + "\n" + "Counter: " +
                counter + "\n" + "Company name: " + this.companyName + "\n" +
                "Country: " + this.country + "\n" + "Package: " + this.packageType + "\n");
    }

    @Override
    public void update() {
        System.out.println("Enter product name: ");
        Scanner in = new Scanner(System.in);
        this.productName = in.nextLine();
        System.out.println("Enter price: ");
        this.price = Double.valueOf(in.nextLine());
        System.out.println("Enter company name: ");
        this.companyName = in.nextLine();
        System.out.println("Enter country: ");
        this.country = in.nextLine();
        System.out.println("Enter package type: ");
        this.packageType = in.nextLine();
    }

    @Override
    public void delete(){
        super.delete();
        this.productID = null;
        this.productName = null;
        this.price = 0;
        this.companyName = null;
        this.country = null;
        this.packageType = null;
    }

    Tea(String productName, double price, String companyName, String country, String packageType){
        this.productID = UUID.randomUUID();
        this.productName = productName;
        this.price = price;
        this.companyName = companyName;
        this.country = country;
        this.packageType = packageType;
    }

    Tea(){
        this.productID = UUID.randomUUID();
        this.productName = "name";
        this.price = 10;
        this.companyName = "company name";
        this.country = "country";
        this.packageType = "package type";
    }
}
