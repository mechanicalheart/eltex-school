package com.company;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class AutomaticOrderGeneration implements Runnable {

    Orders orders;
    public AutomaticOrderGeneration(Orders orders) {
        this.orders = orders;

    }

    private volatile boolean stop = false;

    public void run() {
        while(!stop) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Credentials cred = new Credentials();
            ShoppingCart cart = new ShoppingCart();

            Random rand = new Random();
            int k = rand.nextInt(10);
            for (int i = 0; i < k; i++) {
                Random d = new Random();
                int drinkType = d.nextInt(2);
                if (drinkType == 0) {
                    Tea tea = new Tea();
                    tea.create();
                    cart.add(tea);
                } else {
                    Coffee coffee = new Coffee();
                    coffee.create();
                    cart.add(coffee);
                }
            }

            orders.checkout(cred,cart);
        }
    }

    public void requestStop(){
        stop = true;
    }
}
