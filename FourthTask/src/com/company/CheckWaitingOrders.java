package com.company;
import java.lang.reflect.Executable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class CheckWaitingOrders<T extends List> extends ACheck{

    public CheckWaitingOrders(Orders orders) {
        super(orders);
    }

    public void run(){
        Date date = new Date();
        for (int i = 0; i < o.getOrders().size(); i++) {
            Order order = (Order) o.getOrders().get(i);
            if (order.waitingTime <= date.getTime())
                order.orderStatus = order.orderStatuses[1];
        }
        //System.out.println("checked: ");
        //o.showAllOrders();
    }

}
