package com.company;

import java.util.List;
import java.util.ListIterator;

public class DeleteProcessedOrders<T extends List> extends ACheck{

    public DeleteProcessedOrders(Orders orders) {
        super(orders);
    }

    public void run() {
        ListIterator<T> listIter = o.getOrders().listIterator();
        while (listIter.hasNext()){
            Order ord = (Order) listIter.next();
            if(ord.orderStatus.equals("processed"))
                listIter.remove();
        }
        //System.out.println("deleted: ");
        //o.showAllOrders();
    }
}
