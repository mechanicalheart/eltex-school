package com.company;

import java.util.Collections;

public interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
