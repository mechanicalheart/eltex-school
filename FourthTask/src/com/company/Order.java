package com.company;

import java.util.Date;
import java.util.LinkedList;

public class Order {
    protected String[] orderStatuses = new String[]{"waiting", "processed"};
    protected String orderStatus;
    protected Long creationTime;
    protected Long waitingTime;
    protected Credentials credentials;
    protected ShoppingCart cart;

    public Order(Credentials credentials, ShoppingCart cart) {
        this.credentials = credentials;
        this.cart = cart;
    }
}
