package com.company;

import java.util.Random;
import java.util.UUID;

public class Credentials {
    private UUID id;
    private String lastName;
    private String[] lastNames = new String[]{"Balmont", "Blok", "Gumilyov", "Mandelstam"};
    private String firstName;
    private String[] firstNames = new String[]{"Konstantin", "Alexander", "Nikolay", "Osip"};
    private String patronymic;
    private String[] patronymics = new String[]{"Dmitriyevich", "Alexandrovich", "Stepanovich", "Emilyevich"};
    private String email;
    private String[] emails = new String[]{"cowberry@gmail.com", "nightstreetsthelantern@gmail.com", "giraffe@gmail.com", "leningrad@gmail.com"};

    public Credentials() {
        Random rand = new Random();
        int randnumber = rand.nextInt(4);
        this.id = UUID.randomUUID();
        this.lastName = lastNames[randnumber];
        randnumber = rand.nextInt(4);
        this.firstName = firstNames[randnumber];
        randnumber = rand.nextInt(4);
        this.patronymic = patronymics[randnumber];
        randnumber = rand.nextInt(4);
        this.email = emails[randnumber];
    }
    public void show(){
        System.out.println(lastName + " " + firstName + " " + patronymic + " " + "email: " + email);
    }

}
