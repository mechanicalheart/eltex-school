package com.company;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Tea tea = new Tea();
        tea.create();
        Tea tea2 = new Tea();
        tea2.create();
        Coffee coffee = new Coffee();
        coffee.create();
        Coffee coffee2 = new Coffee();
        coffee2.create();

//        System.out.println(args[1]);

//        for(int i=0; i< Integer.valueOf(args[0]); i++)
//        {
//            if(args[1].equals("Tea")){
//                Tea tea = new Tea();
//                tea.create();
//                tea.read();
//            }
//            else if(args[1].equals("Coffee")){
//                Coffee coffee = new Coffee();
//                coffee.create();
//                coffee.read();
//            }
//
//        }

        //Checking ShoppingCart class
        ShoppingCart cart = new ShoppingCart();
        cart.add(tea);
        cart.add(tea2);
        cart.add(coffee);
        cart.add(coffee2);
        //System.out.println(cart.searchID(tea.productID));
        //System.out.println(cart.searchID(UUID.randomUUID()));
        //cart.showAllObjects();
        cart.delete(coffee2);
        //cart.showAllObjects();
        ShoppingCart cart2 = new ShoppingCart();
        cart2.add(tea);
        cart2.add(coffee);

        Credentials credentials = new Credentials();
        Credentials credentials2 = new Credentials();

//        //Checking Order class
//        Order order = new Order(credentials, cart);
//        Order order2 = new Order(credentials2, cart2);

        //Checking Orders class
        Orders orders = new Orders();
        orders.checkout(credentials, cart);
        orders.checkout(credentials2, cart2);
        TimeUnit.SECONDS.sleep(2);
        orders.checkout(credentials, cart2);
        orders.showAllOrders();
        TimeUnit.SECONDS.sleep(3);
        orders.deleteProcessed();
        System.out.println("Deleting...");
        orders.showAllOrders();

    }
}
