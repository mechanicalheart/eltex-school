package com.company;

import java.sql.Array;
import java.util.*;

public class ShoppingCart {
    private List<Drinks> cart;
    private Set<UUID> generatedIDs;

    public ShoppingCart() {
        this.cart = new ArrayList<Drinks>();
        this.generatedIDs = new HashSet<UUID>();
    }

    public void add(Drinks drink){
        cart.add(drink);
        generatedIDs.add(drink.productID);
    };

    public void delete(Drinks drink){
        cart.remove(drink);
    };

    public boolean searchID(UUID productID){
        if(generatedIDs.contains(productID))
            return true;
        else
            return false;
    }

    public void showAllObjects(){
        for(int i = 0; i < cart.size(); i++) {
            System.out.println("ID: " + cart.get(i).productID);
            System.out.println("Name: " + cart.get(i).productName);
            System.out.println("Price: " + cart.get(i).price);
            System.out.println("Company: " + cart.get(i).companyName);
            System.out.println("Country: " + cart.get(i).country);
            System.out.println("--------------------------");
        }
    }
}
