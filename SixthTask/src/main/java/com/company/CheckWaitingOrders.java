package com.company;
import java.lang.reflect.Executable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CheckWaitingOrders<T extends List> extends ACheck{

    public CheckWaitingOrders(Orders orders) {
        super(orders);
    }

    public void run(){
        while(true) {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Date date = new Date();
            for (int i = 0; i < o.getOrders().size(); i++) {
                Order order = (Order) o.getOrders().get(i);
                if (order.waitingTime <= date.getTime()) {
                    order.orderStatus = order.orderStatuses[1];
                }
            }
        }
    }

}
