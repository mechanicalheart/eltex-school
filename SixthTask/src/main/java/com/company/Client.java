package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;
import java.util.Scanner;

class Client{
    public static void main(String[] args) throws Exception {

        try    {
            System.out.println("Client is running");

            ClientConnectionProcessor c = new ClientConnectionProcessor(Integer.parseInt(args[0]));
            c.run();
        }
        catch(Exception e)    {  System.err.println(e);     }
    }
}