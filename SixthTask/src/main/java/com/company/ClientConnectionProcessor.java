package com.company;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.BindException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class ClientConnectionProcessor extends Thread {

    private int individualPort;

    public ClientConnectionProcessor(int individualPort) {
        this.individualPort = individualPort;
    }

    private static boolean available(int port) {
        System.out.println("--------------Testing port " + port);
        Socket s = null;
        try {
            s = new Socket("wlp7s0",port);
            System.out.println("--------------Port " + port + " is not available");
            return false;
        } catch (IOException e) {
            System.out.println("--------------Port " + port + " is available");
            return true;
        } finally {
            if( s != null){
                try {
                    s.close();
                } catch (IOException e) {
                    throw new RuntimeException("You should handle this error." , e);
                }
            }
        }
    }

    public void run() {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter broadcast port");
        int broadcastPort = 3333; //in.nextInt();
        System.out.println("Enter individual port");
        //int individualPort = 3335; //in.nextInt();
        DatagramPacket pack = new DatagramPacket(new byte[1024],  1024);
        DatagramSocket ds = null;

        System.out.println("Receiver is running");
        try    {
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("Receiving packet");
                ds = new DatagramSocket(broadcastPort);
                ds.receive(pack);
                ds.close();
                System.out.println(ByteBuffer.wrap(pack.getData()).getInt());
                System.out.println(pack.getAddress());
                Socket sock = new Socket(pack.getAddress(), ByteBuffer.wrap(pack.getData()).getInt());
                System.out.println("Socket created");

                ObjectOutputStream outStream = new ObjectOutputStream(sock.getOutputStream());
                Credentials cred = new Credentials();
                ShoppingCart cart = new ShoppingCart();
                Order order = new Order(cred, cart, individualPort);
                outStream.writeObject(order);
                outStream.close();
                sock.close();
                System.out.println("Order sended");

                DatagramSocket indDS = new DatagramSocket(individualPort);
                indDS.receive(pack);
                System.out.println("Order id: " + order.orderID);
                ByteBuffer buffer = ByteBuffer.wrap(pack.getData());
                System.out.println("Processing end time: " + new Date(buffer.getLong()));
                indDS.close();
            }
        }
        catch(Exception e)   { System.out.println(e); }

    }

}
