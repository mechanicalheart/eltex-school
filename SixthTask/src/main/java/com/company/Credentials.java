package com.company;

import java.io.Serializable;
import java.util.Random;
import java.util.UUID;

public class Credentials implements Serializable {
    private UUID id;
    private String lastName;
    private String[] lastNames = new String[]{"Balmont", "Blok", "Gumilyov", "Mandelstam"};
    private String firstName;
    private String[] firstNames = new String[]{"Konstantin", "Alexander", "Nikolay", "Osip"};
    private String patronymic;
    private String[] patronymics = new String[]{"Dmitriyevich", "Alexandrovich", "Stepanovich", "Emilyevich"};
    private String email;
    private String[] emails = new String[]{"cowberry@gmail.com", "nightstreetsthelantern@gmail.com", "giraffe@gmail.com", "leningrad@gmail.com"};

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String[] getLastNames() {
        return lastNames;
    }

    public void setLastNames(String[] lastNames) {
        this.lastNames = lastNames;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String[] getFirstNames() {
        return firstNames;
    }

    public void setFirstNames(String[] firstNames) {
        this.firstNames = firstNames;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String[] getPatronymics() {
        return patronymics;
    }

    public void setPatronymics(String[] patronymics) {
        this.patronymics = patronymics;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String[] getEmails() {
        return emails;
    }

    public void setEmails(String[] emails) {
        this.emails = emails;
    }

    public Credentials() {
        Random rand = new Random();
        int randnumber = rand.nextInt(4);
        this.id = UUID.randomUUID();
        this.lastName = lastNames[randnumber];
        randnumber = rand.nextInt(4);
        this.firstName = firstNames[randnumber];
        randnumber = rand.nextInt(4);
        this.patronymic = patronymics[randnumber];
        randnumber = rand.nextInt(4);
        this.email = emails[randnumber];
    }

    public void show(){
        System.out.println(lastName + " " + firstName + " " + patronymic + " " + "email: " + email);
    }

}
