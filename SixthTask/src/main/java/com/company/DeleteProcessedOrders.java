package com.company;

import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.TimeUnit;

public class DeleteProcessedOrders<T extends List> extends ACheck{

    public DeleteProcessedOrders(Orders orders) {
        super(orders);
    }

    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ListIterator<T> listIter = o.getOrders().listIterator();
            while (listIter.hasNext()) {
                Order ord = (Order) listIter.next();
                if (ord.orderStatus.equals("processed"))
                    listIter.remove();
            }
        }
    }
}
