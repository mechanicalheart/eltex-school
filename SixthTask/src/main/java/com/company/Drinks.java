package com.company;

import java.util.Objects;
import java.util.UUID;

public abstract class Drinks implements ICrudAction {
    protected UUID productID;
    protected String productName;
    protected double price;
    protected static int counter;
    protected String companyName;
    protected String country;

    public UUID getProductID() {
        return productID;
    }

    public void setProductID(UUID productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        Drinks.counter = counter;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public void create() {
        counter++;
    }

    @Override
    public void delete(){
        counter--;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Drinks)) return false;
        Drinks drinks = (Drinks) o;
        return Double.compare(drinks.price, price) == 0 &&
                Objects.equals(productID, drinks.productID) &&
                Objects.equals(productName, drinks.productName) &&
                Objects.equals(companyName, drinks.companyName) &&
                Objects.equals(country, drinks.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productID, productName, price, companyName, country);
    }
}
