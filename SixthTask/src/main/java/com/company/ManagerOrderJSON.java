package com.company;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.gson.Gson;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.UUID;

public class ManagerOrderJSON extends AManageOrder {

    @Override
    public void readById(File file, UUID id, Order order) {

        Orders orders = new Orders();

        // try (FileReader reader = new FileReader(file)) {

        readAll(file,orders);

        // } catch (IOException e){
        //     e.printStackTrace();
        //  }

        order =  orders.getOrderByID(id);

    }

    @Override
    public void saveById(Orders orders, UUID id, File file) {

        ObjectMapper mapper = new ObjectMapper();

        for(int i = 0; i < orders.getSize(); i++) {
            if(orders.getOrder(i).orderID == id) {

                try (FileWriter writer = new FileWriter(file)) {

                    mapper.writeValue(file, orders.getOrder(i));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void readAll(File file, Orders orders) {

        String updated = new String();
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(file.getPath()));
            String str = new String(encoded, Charset.defaultCharset());
            int sizeIndex = str.indexOf("size")+6;
            String del = "{\"orders\":[";
            int size = Character.getNumericValue(str.charAt(sizeIndex));
            String del2 = "],\"size\":" + size + "}";
            updated = str.replace(del, "");
            updated = updated.replace(del2, "");

            String delimiter = "\\Q},{\\E";
            Scanner s = new Scanner(updated).useDelimiter(delimiter);
            ObjectMapper mapper = new ObjectMapper();

            for(int i = 0; i < size;i++){
                String ord = s.next();
                if(i == 0)
                    ord = ord + "}";
                else
                    ord = "{" +ord;
                Order order = mapper.readValue(ord, Order.class);
                orders.checkoutOrder(order);
            }
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void saveAll(Orders orders, File file) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(file, orders);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}