package com.company;

import java.util.Date;
import java.util.LinkedList;
import java.util.UUID;
import java.io.Serializable;

public class Order implements Serializable{
    protected String[] orderStatuses = new String[]{"waiting", "processed"};
    protected String orderStatus;
    protected Long creationTime;
    protected Long waitingTime;
    protected Credentials credentials;
    protected ShoppingCart cart;
    protected UUID orderID;
    protected int clientPort;

    public Order(){

    }

    public String[] getOrderStatuses() {
        return orderStatuses;
    }

    public void setOrderStatuses(String[] orderStatuses) {
        this.orderStatuses = orderStatuses;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public Long getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(Long waitingTime) {
        this.waitingTime = waitingTime;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public void setCart(ShoppingCart cart) {
        this.cart = cart;
    }

    public UUID getOrderID() {
        return orderID;
    }

    public void setOrderID(UUID orderID) {
        this.orderID = orderID;
    }

    public int getClientPort() {
        return clientPort;
    }

    public void setClientPort(int clientPort) {
        this.clientPort = clientPort;
    }

    public Order(Credentials credentials, ShoppingCart cart, int port) {
        this.credentials = credentials;
        this.cart = cart;
        this.orderID = UUID.randomUUID();
        this.clientPort = port;
    }

    public Order(Credentials credentials, ShoppingCart cart) {
        this.credentials = credentials;
        this.cart = cart;
        this.orderID = UUID.randomUUID();
    }

    public Order(Credentials credentials, ShoppingCart cart, UUID id, int port) {
        this.credentials = credentials;
        this.cart = cart;
        this.orderID = id;
        this.clientPort = port;
    }

    public Order(Credentials credentials, ShoppingCart cart, UUID id) {
        this.credentials = credentials;
        this.cart = cart;
        this.orderID = id;
    }
}
