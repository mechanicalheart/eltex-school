package com.company;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;

public class SendMessage extends Thread {
    private int mess;
    private InetAddress h;
    private int p;

    public SendMessage(int message, InetAddress host, int port){

    mess = message;
    h = host;
    p = port;

    }

    public void run() {

        try    {
            while(true) {
                byte[] data = ByteBuffer.allocate(4).putInt(mess).array();
                InetAddress addr = h;
                DatagramPacket pack = new DatagramPacket(data, data.length, addr, p);
                DatagramSocket ds = new DatagramSocket();
                ds.send(pack);
                ds.close();
            }
        }
        catch(Exception e)    { System.err.println(e);  }

    }
}
