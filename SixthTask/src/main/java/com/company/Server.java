package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Server {
    public static void main(String[] args){
        try    {
            System.out.println("Server is running");

            Orders orders = new Orders();
            CheckWaitingOrders check = new CheckWaitingOrders(orders);
            DeleteProcessedOrders delete = new DeleteProcessedOrders(orders);
            Scanner in = new Scanner(System.in);
            System.out.println("Enter interface name");
            String interName = "enp14s0"; //"wlp7s0"; //"enp14s0"; in.nextLine();
            System.out.println("Enter UDP port");
            int portUDP = 3333; //in.nextInt();
            System.out.println("Enter TCP port");
            int portTCP = 3334; //in.nextInt();
            ServerSocket ss = new ServerSocket(portTCP);
            NetworkInterface netwInt= NetworkInterface.getByName(interName);
            InetAddress broadcast = null;
            InetAddress addr = null;
            byte[] data =  ByteBuffer.allocate(4).putInt(portTCP).array();
            for (InterfaceAddress interfaceAddress : netwInt.getInterfaceAddresses())
            {
                broadcast = interfaceAddress.getBroadcast();
                if (broadcast == null)
                    continue;

                System.out.println(interfaceAddress);
                System.out.println(broadcast);
                addr = broadcast;

                //System.out.println("Sending message");

            }

            System.out.println("Checking orders' waiting time");
            Thread ch = new Thread(check);
            ch.start();

            Thread d = new Thread(delete);
            d.start();

            //orders.showAllOrders();
            //ScheduledExecutorService sh = Executors.newSingleThreadScheduledExecutor();
            //sh.scheduleWithFixedDelay(ch, 0, 5, TimeUnit.SECONDS);
            //ScheduledExecutorService sh2 = Executors.newSingleThreadScheduledExecutor();
            //sh2.scheduleWithFixedDelay(d, 0, 5, TimeUnit.SECONDS);

            SendMessage send = new SendMessage(portTCP, addr, portUDP);
            send.start();
            //ScheduledExecutorService sender = Executors.newSingleThreadScheduledExecutor();
            //sender.scheduleWithFixedDelay(send, 0, 5, TimeUnit.SECONDS);

            // Ждет клиентов и для каждого создает отдельный поток
            while (true)         {

                System.out.println("Accepting clients");

                Socket s = ss.accept();
                System.out.println("Client accepted");
                ServerConnectionProcessor p = new ServerConnectionProcessor(s, orders, portUDP);
                p.start();
            }
        }
        catch(Exception e)    { System.out.println(e);     }
    }
}



