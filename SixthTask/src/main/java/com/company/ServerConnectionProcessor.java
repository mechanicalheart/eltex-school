package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectInputStream;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.Scanner;

public class ServerConnectionProcessor extends Thread   {
    private Socket sock;
    private Orders orders;
    private DatagramSocket datSock;
    private int portUDP;
    private boolean checkFlag = false;
    private byte[] data;

    public ServerConnectionProcessor(Socket s, Orders o, int port)     {
        sock = s;
        orders = o;
        portUDP = port;
    }


    public void run()    {
        try        {
            ObjectInputStream inStream = new ObjectInputStream(sock.getInputStream());
            Order order = (Order) inStream.readObject();
            System.out.println("Order accepted");
            orders.checkoutOrder(order);

            while(checkFlag == false){
                if(orders.getOrderByID(order.orderID).orderStatus.equals("processed"))
                    checkFlag = true;
            }
            System.out.println(orders.getOrderByID(order.orderID).orderStatus);
            System.out.println("Order processed");

            ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
            buffer.putLong(orders.getOrderByID(order.orderID).waitingTime);
            data = buffer.array();
            DatagramPacket pack = new DatagramPacket(data, data.length, sock.getInetAddress(), order.clientPort);
            DatagramSocket ds = new DatagramSocket();
            ds.send(pack);
            ds.close();
            System.out.println("Packet sended");

            inStream.close();
            sock.close();
        }
        catch(Exception e)    { System.out.println(e);  }
    }
}