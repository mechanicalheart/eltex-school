package com.company;

import java.util.*;


public class Orders<T extends Order> {
    public static final int WAITING_TIME = 4000;
    private List<T> orders;
    private Map<T, Long> ordersTime;

    public Orders() {
        this.orders = new LinkedList<>();
        this.ordersTime = new HashMap<>();
    }

    public void checkout(Credentials credential, ShoppingCart cart){
        Date date = new Date();
        T order = (T) new Order(credential,cart);
        orders.add(order);
        order.creationTime = date.getTime();
        order.waitingTime = order.creationTime + WAITING_TIME;
        ordersTime.put(order, order.creationTime);
        order.orderStatus = order.orderStatuses[0];
    }

    public void deleteProcessed(){

        ListIterator<T> listIter = orders.listIterator();
        while (listIter.hasNext()){
            Order ord = listIter.next();
            ord.checkWaitingTime();
            if(ord.orderStatus.equals("processed"))
                listIter.remove();
        }

    }

    public void showAllOrders(){
        for(int i = 0; i < orders.size(); i++) {
            System.out.println("Credentials: ");
            orders.get(i).credentials.show();
            Date crT = new Date(orders.get(i).creationTime);
            Date wT = new Date(orders.get(i).waitingTime);
            System.out.println("Creation time: " + crT);
            System.out.println("Waiting time: " + wT);
            System.out.println("Order status: " + orders.get(i).orderStatus);
            System.out.println("--------------------------");
        }
    }

}
