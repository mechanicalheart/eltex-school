package com.eltex.sev;

import com.eltex.sev.model.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class AutomaticOrderGeneration implements Runnable {

    Orders orders;
    public AutomaticOrderGeneration(Orders orders) {
        this.orders = orders;

    }

    private volatile boolean stop = false;

    public void run() {
        Random rand = new Random();
        while(!stop) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Credentials cred = new Credentials();
            ShoppingCart cart = new ShoppingCart();

            int k = rand.nextInt(10);
            for (int i = 0; i < k; i++) {
                Random d = new Random();
                int drinkType = d.nextInt(2);
                Drink drink;
                if (drinkType == 0) {
                    drink = new Tea();
                } else {
                    drink = new Coffee();
                }
                cart.addDrink(drink);
            }

            orders.checkout(cred,cart);
        }
    }

    public void requestStop(){
        stop = true;
    }
}
