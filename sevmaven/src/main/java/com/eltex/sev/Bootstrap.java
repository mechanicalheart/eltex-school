package com.eltex.sev;

import com.eltex.sev.model.*;
import com.eltex.sev.repository.CredentialsRepository;
import com.eltex.sev.repository.ShopOrderRepository;
import com.eltex.sev.repository.ShoppingCartRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;

@Slf4j
@Component
public class Bootstrap implements ApplicationRunner{


    @Autowired
    @Qualifier(value = "filedb")
    File file;

    final
    ShopOrderRepository repo;
    final
    CredentialsRepository credentialsRepository;

    @Autowired
    ShoppingCartRepository shoppingCartRepository;


    @Autowired
    public Bootstrap(ShopOrderRepository repo, CredentialsRepository credentialsRepository) {
        this.repo = repo;
        this.credentialsRepository = credentialsRepository;
    }

    @Transactional
    public void fillDB(){
        Orders orders = new Orders();
        Credentials cred = new Credentials();
        cred.setEmail("kakoi-to@email.com");
        cred.setFirstName("Vasya");
        cred.setLastName("Kotik");
        cred.setPatronymic("QQ");
        ShoppingCart cart = new ShoppingCart();
        ShopOrder shopOrder = new ShopOrder();
        Tea tea1 = new Tea();
        tea1.setPackageType("bags");
        cart.addDrink(tea1);
        shopOrder.setCredentials(cred);
        shopOrder.setShoppingCart(cart);
        Credentials cred2 = new Credentials();
        cred2.setFirstName("Joseph");
        cred2.setLastName("Stalin");
        cred2.setEmail("stalin@ussr.gov");
        ShoppingCart cart2 = new ShoppingCart();
        ShopOrder shopOrder2 = new ShopOrder();
        Coffee coffee = new Coffee();
        coffee.setBeansType("arabica");
        cart2.addDrink(coffee);
        shopOrder2.setShoppingCart(cart2);
        shopOrder2.setCredentials(cred2);
        orders.checkoutOrder(shopOrder2);
        orders.checkoutOrder(shopOrder);
        repo.save(shopOrder);
        repo.save(shopOrder2);
        ManagerOrderJSON m = new ManagerOrderJSON();

        try {
            m.saveAll(orders, file);
        } catch (IOException e) {
            new RuntimeException(e);
        }
    }
    @Override
    public void run(ApplicationArguments args) throws Exception {
        fillDB();
        log.info("Test");
    }
}
