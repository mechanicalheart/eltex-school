package com.eltex.sev;


import com.eltex.sev.model.ShopOrder;
import com.eltex.sev.model.OrderStatus;
import com.eltex.sev.model.Orders;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CheckWaitingOrders<T extends List> extends ACheck{

    public CheckWaitingOrders(Orders orders) {
        super(orders);
    }

    public void run(){
        while(true) {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Date date = new Date();
            for (int i = 0; i < o.getOrders().size(); i++) {
                ShopOrder shopOrder = (ShopOrder) o.getOrders().get(i);
                if (shopOrder.getWaitingTime() <= date.getTime()) {
                    shopOrder.setOrderStatus(OrderStatus.CLOSED);
                }
            }
        }
    }

}
