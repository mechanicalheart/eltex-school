package com.eltex.sev;


public class DeleteException extends RuntimeException {

    private int code;

    public int getCode() {
        return code;
    }

    public DeleteException(String message, int c){

        super(message);
        code = c;
    }
}
