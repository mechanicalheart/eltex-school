package com.eltex.sev;


import com.eltex.sev.model.ShopOrder;
import com.eltex.sev.model.OrderStatus;
import com.eltex.sev.model.Orders;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class DeleteProcessedOrders<T extends List> extends ACheck{

    public DeleteProcessedOrders(Orders orders) {
        super(orders);
    }

    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            o.getOrders().removeIf(x -> ((ShopOrder)x).getOrderStatus().equals(OrderStatus.CLOSED));

        }
    }
}