package com.eltex.sev;


import com.eltex.sev.model.ShopOrder;
import com.eltex.sev.model.Orders;

import java.io.*;
import java.util.UUID;

public class ManagerOrderFile {
    public void readById(Long id, ShopOrder shopOrder, File file) {

        Orders orders = new Orders();

        orders = readOrdersFromFile(file, orders);

        for(int i = 0; i < orders.getOrders().size(); i++){

            if(orders.getOrder(i).getId().equals(id)){
                shopOrder = orders.getOrder(i);
            }
        }

    }

    private Orders readOrdersFromFile(File file, Orders orders) {
        try(FileInputStream fis = new FileInputStream(file)) {
            ObjectInputStream ois = new ObjectInputStream(fis);
            orders = (Orders)ois.readObject();
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return orders;
    }

    public void readAll(Orders orders, File file) {

        orders = readOrdersFromFile(file, orders);

    }

    public void saveById(Orders orders, UUID id, File file) {

        for(int i = 0; i < orders.getOrders().size(); i++) {

            if(orders.getOrder(i).getId().equals(id)){

                try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
                    oos.writeObject(orders.getOrder(i));
                }
                catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }

        }

    }

    public void saveAll(Orders orders, File file) {

        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(orders);
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

}