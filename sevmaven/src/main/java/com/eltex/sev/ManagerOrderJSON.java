package com.eltex.sev;


import com.eltex.sev.model.ShopOrder;
import com.eltex.sev.model.Orders;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.UUID;

public class ManagerOrderJSON {

    public void readById(UUID id, ShopOrder shopOrder, File file) {

        Orders orders = readAll(file);
        shopOrder =  orders.getOrderByID(id);

    }

    public void saveById(Orders orders, UUID id, File file) {

        ObjectMapper mapper = new ObjectMapper();

        for(int i = 0; i < orders.getOrders().size(); i++) {
            if(orders.getOrder(i).getId().equals(id)) {

                try (FileWriter writer = new FileWriter(file)) {

                    mapper.writeValue(file, orders.getOrder(i));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

/*    public void readAll(Orders orders, File file) {

        String updated = new String();
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(file.getPath()));
            String str = new String(encoded, Charset.defaultCharset());
            int sizeIndex = str.indexOf("size")+6;
            String del = "{\"orders\":[";
            int size = Character.getNumericValue(str.charAt(sizeIndex));
            String del2 = "],\"size\":" + size + "}";
            updated = str.replace(del, "");
            updated = updated.replace(del2, "");

            String delimiter = "\\Q},{\"orderStatuses\":\\E";
            Scanner s = new Scanner(updated).useDelimiter(delimiter);
            ObjectMapper mapper = new ObjectMapper();

            for(int i = 0; i < size;i++){
                String ord = s.next();
                if(i == 0)
                    ord = ord + "}";
                else
                    ord = "{\"orderStatuses\":" +ord;
                ShopOrder shopOrder = mapper.readValue(ord, ShopOrder.class);
                orders.checkoutOrder(shopOrder);
            }
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }*/

    public Orders readAll(File file){

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(file, Orders.class);
        }
        catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    public void saveAll(Orders orders, File file) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(file, orders);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}