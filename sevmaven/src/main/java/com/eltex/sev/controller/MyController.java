package com.eltex.sev.controller;
import com.eltex.sev.DeleteException;
import com.eltex.sev.ManagerOrderJSON;
import com.eltex.sev.model.Orders;
import com.eltex.sev.service.ShopService;
import com.eltex.sev.service.OrderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Controller
public class MyController {

    @Autowired
    OrderService orderService;
    @Autowired
    ShopService m;
    @GetMapping("/com/eltex/sev")
    public String printCommands(@RequestParam(value = "command", required = true) String param1,
                                @RequestParam(value = "order_id", required = false) String param2,
                                @RequestParam(value = "cart_id", required = false) String param3, Model model, ServletRequest request) throws DeleteException {
        Map<String, String[]> paramMap = request.getParameterMap();

        String message;


//        if(param1.equals("createorders") && !paramMap.containsKey("order_id") && !paramMap.containsKey("cart_id")){
//            message = m.createOrders();
//            model.addAttribute("message", message);
//            log.info("Orders has been created");
//            return "orders";
//
//        }

        if(param1.equals("readall") && !paramMap.containsKey("order_id") && !paramMap.containsKey("order_id")){
            Orders orders = m.readFromDB();
            model.addAttribute("message", orders.toString());
            model.addAttribute("orders", orders);
            log.info("Orders has been read");
            return "orders";
        }

        if(param1.equals("readbyid") && paramMap.containsKey("order_id") && !paramMap.containsKey("cart_id")){

            //try {
            //message = m.readByIdFromFile(UUID.fromString(param2));
            Orders orders = m.readFromDB();
            message = m.readByIdFromDB(UUID.fromString(param2));
            log.info("ShopOrder has been read");
            model.addAttribute("message", message);
            model.addAttribute("orders", orders);
            return "orders";
//            } catch (JsonProcessingException e) {
//                throw new RuntimeException(e);
//            }

        }

        if(param1.equals("addtocart") && !paramMap.containsKey("order_id") && paramMap.containsKey("cart_id")){
            //message = m.addToCart(UUID.fromString(param3));
            message = m.addToCart(UUID.fromString(param3));
            Orders orders = m.readFromDB();
            model.addAttribute("message", message);
            model.addAttribute("orders", orders);
            log.info("Item has been added to drinks");
            return "orders";
        }


        if(param1.equals("delbyid") && paramMap.containsKey("order_id") && !paramMap.containsKey("cart_id")){
            m.delById(UUID.fromString(param2));
            Orders orders = m.readFromDB();
            model.addAttribute("orders", orders);
            try {
                message = "ShopOrder deleted";
                model.addAttribute("message", message);
                log.info("ShopOrder has been deleted");
                return "orders";
            } catch (DeleteException e) {
                message = e.getMessage();
                model.addAttribute("message", message);
                log.info(e.getMessage());
                return "com/eltex/sev";
            }

        }

        log.info("Wrong command");
        throw new DeleteException("Wrong command", 3);

    }

}