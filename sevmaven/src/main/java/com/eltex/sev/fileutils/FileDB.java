package com.eltex.sev.fileutils;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class FileDB {

    @Value("${eltex.sev.filename}")
    String fileName;


    @Bean
    @Qualifier(value = "filedb")
    File openFile(){
        return new File("orders.json");
    }
}
