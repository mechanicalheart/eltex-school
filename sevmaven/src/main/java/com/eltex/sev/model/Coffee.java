package com.eltex.sev.model;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
public class Coffee extends Drink {
    private String beansType;

    /*@Override
    public String toString() {
        return String.format("Coffee with % beans", beansType);
    }*/
}