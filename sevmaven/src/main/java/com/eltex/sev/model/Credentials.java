package com.eltex.sev.model;


import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Random;
import java.util.UUID;

@Data
@Entity
public class Credentials extends BaseEntity {
    private String lastName;
    private String firstName;
    private String patronymic;
    private String email;

    @Override
    public String toString() {
        return String.format("%s %s %s", firstName, lastName, email);
    }
}