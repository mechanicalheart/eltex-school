package com.eltex.sev.model;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Tea.class, name = "tea"),
        @JsonSubTypes.Type(value = Coffee.class, name = "coffee")
})

//@JsonDeserialize(as = Tea.class)

@Data
public abstract class Drink extends BaseEntity {
    private String productName;
    private Double price;
    private String companyName;
    private String country;
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "drink_id")
//    private ShoppingCart shoppingCart;
}