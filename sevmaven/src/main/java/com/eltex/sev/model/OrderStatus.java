package com.eltex.sev.model;

public enum OrderStatus {
    OPENED, CLOSED;

    OrderStatus() {
    }
}
