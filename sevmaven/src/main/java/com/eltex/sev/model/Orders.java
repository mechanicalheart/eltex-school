package com.eltex.sev.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.*;

public class Orders<T extends ShopOrder> implements Serializable {

    private List<T> orders = new LinkedList();

    @JsonIgnore
    static final int WAITING_TIME = 4000;

    public static int getWaitingTime() {
        return WAITING_TIME;
    }

    public void setOrders(List<T> orders) {
        this.orders = orders;
    }

    public Orders() {
    }

    public Orders(List<T> orders){
        this.orders = orders;
    }

    public void checkout(Credentials credential, ShoppingCart cart) {
        Date date = new Date();
        T order = (T) new ShopOrder();
        order.setCredentials(credential);
        order.setShoppingCart(cart);
        this.orders.add(order);
        order.setCreationTime(date.getTime());
        order.setCreationTime(order.getCreationTime() + 4000L);
        //this.ordersTime.put(order, order.creationTime);
        order.setOrderStatus(OrderStatus.OPENED);
    }

    public void checkoutOrder(ShopOrder shopOrder) {
        Date date = new Date();
        this.orders.add((T) shopOrder);
        shopOrder.setCreationTime(date.getTime());
        shopOrder.setWaitingTime(shopOrder.getCreationTime() + 4000L);
        //this.ordersTime.put((T) shopOrder, shopOrder.creationTime);
        shopOrder.setOrderStatus(OrderStatus.OPENED);
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        orders.forEach(builder::append);
        return builder.toString();
    }


    public ShopOrder getOrder(int i) {
        return (ShopOrder)this.orders.get(i);
    }

    public List<T> getOrders() {
        return this.orders;
    }

    public ShopOrder getOrderByID(UUID id){
        Optional<T> first = orders.stream().filter(x -> x.getId().equals(id)).findFirst();
        return first.orElse(null);
    }

    public void deleteOrderById(UUID id) {
        ShopOrder shopOrder = getOrderByID(id);
        if(shopOrder != null)
            orders.remove(shopOrder);
    }
}