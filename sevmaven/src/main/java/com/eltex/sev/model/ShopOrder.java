package com.eltex.sev.model;


import lombok.Data;

import javax.persistence.*;
import java.util.*;
import java.io.Serializable;


@Data
@Entity
public class ShopOrder extends BaseEntity{
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;
    private Long creationTime;
    private Long waitingTime;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Credentials credentials;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private ShoppingCart shoppingCart;
    private int clientPort;

    @Override
    public String toString(){
        return String.format("Credentials: \n %s\nCreation time:%s\nWaiting tile:%s\n" +
                        "Order status:%s" +
                        "\"--------------------------\"",
        credentials, new Date(creationTime), new Date(waitingTime), orderStatus);
    }
}
