package com.eltex.sev.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Data
@Entity
public class ShoppingCart extends BaseEntity {
    @JsonInclude()
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "shopping_cart_id")
    private List<Drink> drinks = new LinkedList<>();

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "shoppingCart", fetch = FetchType.LAZY)
    private ShopOrder shopOrder;


    public void addDrink(Drink drink){
        drinks.add(drink);
    }

    public String showDrinks(){
        StringBuilder builder = new StringBuilder();
        drinks.forEach(builder::append);
        return builder.toString();
    }
    public void showAllObjects(){
        for(int i = 0; i < drinks.size(); i++) {
            System.out.println("ID: " + drinks.get(i).getId());
            System.out.println("Name: " + drinks.get(i).getProductName());
            System.out.println("Price: " + drinks.get(i).getPrice());
            System.out.println("Company: " + drinks.get(i).getCompanyName());
            System.out.println("Country: " + drinks.get(i).getCountry());
            System.out.println("--------------------------");
        }
    }
}