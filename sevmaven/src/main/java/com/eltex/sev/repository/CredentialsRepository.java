package com.eltex.sev.repository;

import com.eltex.sev.model.Credentials;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CredentialsRepository extends JpaRepository<Credentials, UUID> {
}
