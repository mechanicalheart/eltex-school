package com.eltex.sev.service;

import com.eltex.sev.model.Drink;
import com.eltex.sev.repository.DrinkRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class AlcoService extends BaseService<Drink, UUID> {
    JpaRepository repository;

    public AlcoService(@Autowired DrinkRepository repository) {
        this.repository = repository;
    }
}
