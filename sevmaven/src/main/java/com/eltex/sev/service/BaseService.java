package com.eltex.sev.service;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public abstract class BaseService<T, K> {
    JpaRepository<T, K> repository;

    public void save(T t){
        repository.save(t);
    }

    public void saveAll(Iterable<T> iterable){
        repository.saveAll(iterable);
    }

    public void delete(T t){
        repository.delete(t);
    }

    public void deleteById(K id){
        repository.deleteById(id);
    }

    public void deleteAll(){
        repository.deleteAll();
    }

    public Optional<T> findById(K id){
        return repository.findById(id);
    }

    public List<T> findAll(){
        return repository.findAll();
    }

    public JpaRepository getRepository(){
        return repository;
    }
}
