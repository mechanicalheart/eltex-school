package com.eltex.sev.service;

import com.eltex.sev.model.Credentials;
import com.eltex.sev.repository.CredentialsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CredentialsService extends BaseService<Credentials, UUID> {
    JpaRepository repository;

    public CredentialsService(@Autowired CredentialsRepository repository) {
        this.repository = repository;
    }
}
