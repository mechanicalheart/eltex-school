package com.eltex.sev.service;

import com.eltex.sev.model.ShopOrder;
import com.eltex.sev.repository.ShopOrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class OrderService extends BaseService<ShopOrder, UUID> {
    public OrderService(@Autowired ShopOrderRepository orderRepository) {
        this.repository = orderRepository;
    }

}
