package com.eltex.sev.service;


import com.eltex.sev.DeleteException;
import com.eltex.sev.ManagerOrderJSON;
import com.eltex.sev.model.*;
import com.eltex.sev.repository.ShopOrderRepository;
import com.eltex.sev.repository.ShoppingCartRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;


@Service
public class ShopService {
    File file;
    private ShopOrderRepository orderRepository;
    private ShoppingCartRepository cartRepository;


    @Autowired
    public ShopService(@Qualifier(value = "filedb") File file,
                       ShopOrderRepository orderRepository, ShoppingCartRepository cartRepository) {
        this.file = file;
        this.orderRepository = orderRepository;
        this.cartRepository = cartRepository;
    }

    public String readFromFile() throws JsonProcessingException {
        ManagerOrderJSON m = new ManagerOrderJSON();
        Orders orders = m.readAll(file);
        if(orders.getOrders().size() == 0){
            return "Orders not found";
        }
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(orders);
    }

//    public String readFromDB(){
//        List<ShopOrder> ordersList = orderRepository.findAll();
//        if(ordersList.size() == 0){
//            return "Orders not found";
//        }
//        Orders orders = new Orders(ordersList);
//        return orders.toString();
//    }

    public Orders readFromDB(){
        Orders orders;
        List<ShopOrder> ordersList = orderRepository.findAll();

      return orders = new Orders(ordersList);
    }

    public String readByIdFromFile(UUID id) throws JsonProcessingException {

        ManagerOrderJSON m = new ManagerOrderJSON();
        Orders orders = m.readAll(file);
        ShopOrder shopOrder = orders.getOrderByID(id);
        if(shopOrder == null){
            return "Order not found";
        }
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(shopOrder);

    }

    public String readByIdFromDB(UUID id){
        Optional<ShopOrder> result = orderRepository.findById(id);
        return result.isPresent() ? result.get().toString() : "Not found";

    }

    public String addToCart(UUID id) {

        Random r = new Random();

//        Optional<ShopOrder> orderOpt = orderRepository.findById(id);
//
//        if(!orderOpt.isPresent()){
//            return "Order not found";
//        }

        Optional<ShoppingCart> cart = cartRepository.findById(id);
        ShoppingCart sc = cart.get();

        Drink drink;
        if(r.nextInt(2) == 0) {
            drink = new Tea();
        }
        else {
            drink = new Coffee();
        }

        sc.addDrink(drink);

        cartRepository.save(sc);
        //Orders orders = readFromDB();
        ManagerOrderJSON m = new ManagerOrderJSON();
        File f = new File("orders.json");
//        try {
//            m.saveAll(orders, f);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        Orders orders = m.readAll(file);

        for(int i = 0; i < orders.getOrders().size(); i++) {
            if(orders.getOrder(i).getShoppingCart().getId().equals(id)){
                orders.getOrder(i).getShoppingCart().addDrink(drink);
                try {
                    m.saveAll(orders, file);
                } catch (IOException e) {
                     throw new RuntimeException(e);
                }
                return String.format("Id :%s", drink.getId());
            }
        }
        return "ShopOrder doesn't exist";
    }

    public int delById(UUID id) throws DeleteException {
        long countBefore = orderRepository.count();
        orderRepository.deleteById(id);
        long countAfter = orderRepository.count();
        if(countBefore == countAfter){
            throw new DeleteException("ShopOrder doesn't exist in db", 1);
        }

        Orders orders =new Orders();
        ManagerOrderJSON m = new ManagerOrderJSON();

        orders = m.readAll(file);
        for(int i = 0; i < orders.getOrders().size(); i++) {
            if(orders.getOrder(i).getId().equals(id)){
                orders.deleteOrderById(id);
                return 0;
            }
        }

        throw new DeleteException("ShopOrder doesn't exist in file", 1);
    }

}
