# Web application with Spring Security and Angular

This is a small web application with two pages - authorization page and main page.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

Things you need to install:

* **Java 1.8.0_201**
* **Apache Maven**
* **Node.js**
* **npm**
* **Angular CLI**

Install Java by typing this in terminal: 
```
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
```
Check installed version:
```
java -version
javac -version
```

Install Apache Maven:
```
sudo apt-get install mvn
# if that does not work, try
sudo apt-get install maven
```

Install Node.js:
```
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```

npm client command line interface is installed with Node.js by default.

To install the CLI using npm enter the following command:
```
sudo npm install -g @angular/cli
```

### Installing

To install and run this application you need to follow these steps:

* Download repo from my page or clone it by typing this in terminal:

```
git clone https://mechanicalheart@bitbucket.org/mechanicalheart/eltex-school.git
```

*Open the /spring-security-angular directory.
* Go into /server directory and run project:
```
mvn spring-boot:run
```
* Then go into /client directory and use the following command:
```
ng serve --open
```
* Open in browser http://localhost:4200/
* Type username and password in form. Username and password are stored in /server/credentials file, you can change it by running server project with CreateFile.java configuration and typing credentials in console. Default username is "user" and default password is "password".  
* You are done! 


## Built With

* [Intellij IDEA Ultimate](https://www.jetbrains.com/idea/) - The IDE used
* [Apache Maven 3.5.2](https://maven.apache.org/) - Dependency Management
* [Angular6](https://angular.io/) - The JS framework used
* [Spring](https://spring.io/) - The Java framework used
* [Java 1.8.0_201](https://java.com/ru/download/)
* [Node.js v8.10.0](https://nodejs.org/en/) 

## Authors

[**Anastasiya Lebedeva**](https://bitbucket.org/mechanicalheart)

## Acknowledgments

* Special thanks to my two cats for mental support

