﻿import {User} from '../user.interface';
import {UserService} from '../user.service';
import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {Router, ActivatedRoute} from '@angular/router';
import {environment, SERVER_URL} from '../../environments/environment';
import ProgressBar from 'progressbar.js';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {

  user: User = {userName: null, password: null, result: null, progress: null};
  userName: string;
  result: string;
  circleBar: ProgressBar;
  statee: number;
  progress: number;

  constructor(private http: HttpClient,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService) {
  }

  submitted = false;

  ngOnInit() {
    this.userService.getUser().subscribe(
      value => {
        this.user = value;
        if (this.user.userName == null) {
          this.user.userName = localStorage.getItem('userName');
          this.userService.create(this.user);
          if (this.user.result != '')
            this.result = this.user.result;
        }
        this.progress = Number(this.user.progress);
        if (Number(this.user.progress))
          this.test();
        console.log('[GET] get User successfully', value);
      }, error => {
        console.log('FAIL to get User!', error);
      },
      () => {
        console.log('GET User - now completed.');
      });


    if (localStorage.getItem('token')) {
      let url = SERVER_URL + '/user';
      let headers: HttpHeaders = new HttpHeaders({
        'Authorization': 'Basic ' + localStorage.getItem('token')
      });
      let options = {headers: headers};
      this.http.post<Observable<Object>>(url, {}, options).subscribe(principal => {
          this.userName = principal['name'];
        },
        error => {
          if (error.status == 401)
            alert('Unauthorized');
        }
      );
    } else {
      alert('Unathorized');
      this.router.navigate(['login']);
    }


  }


  logout() {
    localStorage.setItem('token', '');
    if (Number(this.user.progress) > 0 && Number(this.user.progress) < 1) {
      this.user.progress = null;
      this.circleBar.stop();
      this.userService.update(this.user);
    }
  }

  test() {
    this.submitted = true;
    this.userService.getUser().subscribe(
      value => {
        this.user = value;
        if (this.user.userName == null) {
          this.user.userName = localStorage.getItem('userName');
          this.userService.create(this.user);
        }
        console.log('[GET] get User successfully', value);
      }, error => {
        console.log('FAIL to get User!', error);
      },
      () => {
        console.log('GET User - now completed.');
      });

    this.circleBar = new ProgressBar.Circle("#circle-container", {
      color: "white",
      strokeWidth: 23,
      trailWidth: 25,
      trailColor: "grey",
      from: {color: "#00F260"},
      to: {color: "#0575E6"},
      text: {
        value: '0',
        className: 'progress-text',
        style: {
          color: 'black',
          position: 'absolute',
          top: '110%',
          left: '16%',
          padding: 0,
          margin: 0,
          transform: null
        }
      },
      step: (state, shape) => {
        shape.path.setAttribute("stroke", state.color);
        shape.setText('Осталось ' + (30 - Math.round(shape.value() * 30)) + ' секунд');
        this.user.progress = shape.value();
        this.userService.update(this.user);
        if (shape.value() == 1 && this.user.result == null) {
          let rand = 0;
          do {
            rand = 1002 - 0.5 + Math.random() * (1010 - 1002 + 1)
            rand = Math.round(rand);
          } while (rand == 1007 || rand == 1009)
          this.user.result = 'Результат теста линии: ' + rand + ' импульсов';
          this.userService.update(this.user);
          this.userService.getUser().subscribe(
            value => {
              this.user = value;
              while(this.result == null)
                this.result = this.user.result;
              console.log('[GET] get User successfully', value);
            }, error => {
              console.log('FAIL to get User!', error);
            },
            () => {
              console.log('GET User - now completed.');
            });

        } else
          this.userService.getUser().subscribe(
            value => {
              this.user = value;
              this.result = this.user.result;
              console.log('[GET] get User successfully', value);
            }, error => {
              console.log('FAIL to get User!', error);
            },
            () => {
              console.log('GET User - now completed.');
            });

      }
    });

    this.circleBar.set(this.progress);
    let d = 30000 - 30000 * this.progress;
    if (this.progress == 1)
      this.circleBar.animate(1, {
        duration: 1
      });
    else
      this.circleBar.animate(1, {
        duration: d
      });
  }

  clear() {
    this.submitted = false;
    this.circleBar.destroy();
    this.result = null;
    this.progress = null;
    this.user.result = null;
    this.user.progress = null;
    this.userService.update(this.user);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  };


}
