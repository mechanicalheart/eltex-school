﻿import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment, SERVER_URL} from '../../environments/environment';
import {sha256, sha224} from 'js-sha256';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  model: any = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) {
  }

  ngOnInit() {
    if (localStorage.getItem('token'))
      this.router.navigate(['home']);
  }

  login() {
    let url = SERVER_URL + '/login';
    var pass = sha256.hex(this.model.password);
    this.http.post<Observable<boolean>>(url, {
      userName: this.model.username,
      password: pass
    }).subscribe(isValid => {
      if (isValid) {
        localStorage.setItem('token', btoa(this.model.username + ':' + pass));
        localStorage.setItem('userName', this.model.username);
        this.router.navigate(['home']);
      } else {
        alert("Authentication failed.")
      }
    });
  }
}
