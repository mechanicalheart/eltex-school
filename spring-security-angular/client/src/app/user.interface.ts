export interface User {
    userName: string;
    password: string;
    result: string;
    progress: string;
}
