import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment, SERVER_URL} from '../environments/environment';

import {User} from './user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private static readonly USER_URL = SERVER_URL + '/api/user';
  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {
  }

  createUser(user: User): Observable<Object> {
    return this.http.post(UserService.USER_URL, user, {headers: this.headers});
  }

  updateUser(user: User): Observable<any> {
    return this.http.put(UserService.USER_URL, user, {headers: this.headers});
  }

  getUser(): Observable<any> {
    return this.http.get(UserService.USER_URL, {headers: this.headers});
  }

  create(user: User) {
    this.createUser(user).subscribe(
      value => {
        console.log('[POST] create User successfully', value);
      }, error => {
        console.log('FAIL to create User!', error);
      },
      () => {
        console.log('POST User - now completed.');
      });

  }

  update(user: User) {
    this.updateUser(user).subscribe(
      value => {
        console.log('[PUT] update User successfully', value);
      }, error => {
        console.log('FAIL to update User!', error);
      },
      () => {
        console.log('PUT User - now completed.');
      });

  }

  get(user: User) {
    this.getUser().subscribe(
      value => {
        user = value;
        console.log('[GET] get User successfully', value);
      }, error => {
        console.log('FAIL to get User!', error);
      },
      () => {
        console.log('GET User - now completed.');
      });

  }

}
