package com.mechheart.server.basicauth;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;
import java.io.*;
import java.io.IOException;

/***
 * This class creates a file with a username and a hashed password.
 * The password is hashing with SHA256 algorithm.
 */
public class CreateFile {

    /**
     * Main method, starts creating the file.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        /**
         * Reader for input from console.
         * */
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter username: ");

        /**
         * String for storing input from console.
         * */
        String s = br.readLine();

        /** Username using for logging in. */
        String username = s;

        System.out.print("Enter password: ");

        s = br.readLine();

        /** Password using for logging in. */
        String password = s;

        /** File where username and password will be written. */
        File f = new File("credentials");

        /** String for hashed password. */
        String sha256hex = DigestUtils.sha256Hex(password);

        /** Writing username and password in the file. */
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(f), "utf-8"))) {
            writer.write("username:" + username + "\n" + "password:" + sha256hex);
        }
    }
}
