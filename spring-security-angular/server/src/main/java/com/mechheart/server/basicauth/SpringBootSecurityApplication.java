package com.mechheart.server.basicauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class. It runs the application, declares @Bean methods
 * and also triggers auto-configuration and component scanning.
 * */
@SpringBootApplication(scanBasePackages = "com.mechheart.server")
public class SpringBootSecurityApplication {

    /**
     * Main method, runs the application.
     * @param args
     * */
    public static void main(String[] args) {
        SpringApplication.run(SpringBootSecurityApplication.class, args);
    }
}
