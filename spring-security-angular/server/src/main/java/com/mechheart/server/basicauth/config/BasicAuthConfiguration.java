package com.mechheart.server.basicauth.config;

import com.mechheart.server.vo.FReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.io.File;

/**
 * This class is using for configuring authentication and web security.
 * */
@Configuration
@EnableWebSecurity
public class BasicAuthConfiguration extends WebSecurityConfigurerAdapter {

    /**
     * This method configures an AuthenticationManager which attempts to authenticate the passed
     * Authentication object (Principal) and returns a fully populated Authentication object
     * (including granted authorities) if successful.
     * @param auth is AuthenticationManagerBuilder which creates AuthenticationManager.
     * @throws Exception
     * */
        @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        File f = new File("credentials");
        FReader r = new FReader(f);
        r.setMap();

        auth
                .inMemoryAuthentication()
                .withUser(r.getMap().get("username"))
                .password(r.getMap().get("password"))
                .roles("USER");

    }

    /**
     * This method configures web based security for specific http requests.
     * @param http is object of HttpSecurity class which is using for configuring
     * web based security.
     * @throws Exception
     * */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers("/api/user").permitAll()
                .antMatchers("/login").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();
    }


}
