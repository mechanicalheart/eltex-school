package com.mechheart.server.controller;

import java.io.*;
import java.security.Principal;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mechheart.server.vo.FReader;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.mechheart.server.vo.User;

/**
 * Controller class.
 * Controller handles incoming requests and updates both models and views.
 */
@RestController
@CrossOrigin
public class UserController {
    /**
     * This object is using for storing information about current state of a page.
     */
    User u = new User();

    /**
     * This method handles HTTP request to a /login page. It checks if submitted credentials
     * and credentials from file are equal.
     *
     * @param user is object of User class where submitted credentials are stored.
     * @return <code>true</code> if credentials are equal and <code>false</code> if not.
     * @throws IOException
     */
    @RequestMapping("/login")
    public boolean login(@RequestBody User user) throws IOException {
        File f = new File("credentials");
        FReader r = new FReader(f);
        r.setMap();
        return user.getUserName().equals(r.getMap().get("username")) && user.getPassword().equals(r.getMap().get("password"));
    }

    /**
     * This method handles HTTP request to a /user page. It takes submitted credentials and stores it in
     * Principal which is class for storing information about currently logged user.
     *
     * @param request is request with submitted credentials.
     * @return user which is object where username and password are stored.
     */
    @RequestMapping("/user")
    public Principal user(HttpServletRequest request) {
        String authToken = request.getHeader("Authorization").substring("Basic".length()).trim();
        return () -> new String(Base64.getDecoder().decode(authToken)).split(":")[0];
    }

    /**
     * This method is using for storing information about current state of a page.
     * @param user is object with information.
     * @return u is object with that has been saved.
     */
    @PostMapping("/api/user")
    public User createUser(@RequestBody User user){
        u = user;
        return u;
    }

    /**
     * This method is using for sending information about current state of a page to a client.
     * @return u is object with information.
     */
    @GetMapping("/api/user")
    public User getUser(){
        return u;
    }

    /**
     * This method is using for updating information about current state of a page to a client.
     * @param user is object with new information.
     * @return u is object that has been updated.
     */
    @PutMapping("/api/user")
    public User updateUser(@RequestBody User user){
        if(u.getResult() != null && user.getProgress() != null)
            return u;
        u = user;
        return u;
    }


}
