package com.mechheart.server.vo;

import java.io.*;
import java.util.HashMap;

/**
 * This class is using for extracting password and username that were written
 * in file and then comparing them with ones that were submitted by user via client.
 * */
public class FReader {

    /**
     * This parameter is hashmap that is using for storing
     * username as key and password as value.
     * */
    private HashMap<String, String> map;

    /**
     * This parameter is an abstract representation of file and directory pathnames.
     * It refers to the file where username and password are stored.
     * */
    private File f;

    /**
     * Constructor.
     * @param f is name of the file where username and password are stored.
     * */
    public FReader(File f) {
        this.f = f;
    }

    /**
     * This method is using for getting parameter {@link FReader#map}. Elements of
     * this map are comparing with ones that were submitted by user via client.
     * @return map
     * */
    public HashMap<String, String> getMap() {
        return map;
    }

    /**
     * This method is using for storing username and password that were extracted
     * from the file in hashmap.
     * @throws IOException
     * */
    public void setMap() throws IOException {
        this.map = extractCredentials(f);
    }

    /**
     * This method is using for extracting username and password from the file.
     * @param f is name of the file where username and password are stored.
     * @return m. It is a hashmap and is using for transferring values to
     * {@link FReader#map}
     * @throws IOException
     * */
    private HashMap<String, String> extractCredentials(File f) throws IOException {
        HashMap<String, String> m = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(f))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(":", 2);
                if (parts.length >= 2) {
                    String key = parts[0];
                    String value = parts[1];
                    m.put(key, value);
                } else {
                    System.out.println("ignoring line: " + line);
                }
            }
        }
        return m;
    }

}
