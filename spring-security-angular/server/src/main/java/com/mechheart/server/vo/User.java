package com.mechheart.server.vo;

import java.io.Serializable;

/**
 * User object class. Object of this class is using for storing username
 * and password that were submitted by the user using the client.
 * */
public class User implements Serializable {

    /**
     * Username parameter is using for storing username
     * that was submitted by the user.
     * */
    private String userName;

    /**
     * Password parameter is using for storing password
     * that was submitted by the user.
     * */
    private String password;

    /**
     * Result parameter is using for storing result
     * of the test.
     * */
    private String result;

    /**
     * Progress parameter is using for storing current progress
     * of the test.
     * */
    private String progress;

    /**
     * This method is using for getting parameter {@link User#userName}
     * @return username
     * */
    public String getUserName() {
        return userName;
    }

    /**
     * This method is using for setting parameter {@link User#userName}
     * @param userName is username that were submitted by the user.
     * */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * This method is using for getting parameter {@link User#password}
     * @return password
     * */
    public String getPassword() {
        return password;
    }

    /**
     * This method is using for setting parameter {@link User#password}
     * @param password is password that were submitted by the user.
     * */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * This method is using for setting parameter {@link User#result}
     * @return result
     * */
    public String getResult() {
        return result;
    }

    /**
     * This method is using for setting parameter {@link User#result}
     * @param result is result of the test.
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * This method is using for getting parameter {@link User#progress}
     * @return progress
     */
    public String getProgress() {
        return progress;
    }

    /**
     * This method is using for setting parameter {@link User#progress}
     * @param progress is current progress of the test.
     */
    public void setProgress(String progress) {
        this.progress = progress;
    }

}
